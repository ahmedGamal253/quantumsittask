//
//  UITextViewExtention.swift
//  IQRAA
//
//  Created by Admin on 3/7/19.
//  Copyright © 2019 internet plus. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    func stringFromHtml(htmlString: String) {
        let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        self.attributedText = attributedString
    }

}
