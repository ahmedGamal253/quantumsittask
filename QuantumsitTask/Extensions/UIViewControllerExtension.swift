//
//  UIViewControllerExtension.swift
//  Pharmacist
//
//  Created by mohamed elmaazy on 7/9/18.
//  Copyright © 2018 Ahmed gamal. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    
    func showAlart(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showIndicator(_ show: Bool = true) {
        if show {
            let size = CGSize(width:70, height: 70)
            startAnimating (size , type: .ballBeat, color: .orange, padding: 10)
        } else {
            stopAnimating()
        }
    }
}
