//
//  AboutUsResponseModel.swift
//  QuantumsitTask
//
//  Created by jimmy on 11/27/20.
//  Copyright © 2020 jimmy. All rights reserved.
//

import Foundation

class AboutUsResponseModel: Codable{
    let InnerData: [AboutModel]?
}

class AboutModel: Codable{
    let id: Int?
    let content: String?
}
