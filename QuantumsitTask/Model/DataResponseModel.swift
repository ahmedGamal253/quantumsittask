//
//  DataResponseModel.swift
//  QuantumsitTask
//
//  Created by jimmy on 11/27/20.
//  Copyright © 2020 jimmy. All rights reserved.
//

import Foundation

// MARK: - UserResponsModel
struct DataResponsModel: Codable {
    let message: String?
    let status: Bool?
    let innerData: InnerData?

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case status = "Status"
        case innerData = "InnerData"
    }
}

// MARK: - InnerData
struct InnerData: Codable {
    let token: String?
    let user: User?
}

// MARK: - User
struct User: Codable {
    let id, roleID: Int?
    let fullName, name, email, photoURL: String?
    let createdAt, updatedAt, mobileNo: String?
    let age, gender, organizationID: Int?
    let location, checkInLocation, checkOutLocation: String?
    let busID, stopPointID, stopPointIDBack: Int?
    let identification, position: String?
    let tempStatus, checkIOOrg, isLoggedIn: Int?
    let hasCheckedIn, hasCheckedOut, hasAttended, hasAttendedBack: Bool?
    let hasAttendedBySupervisor, hasAttendedBackBySupervisor, hasArrivedBySupervisor: Bool?
    let photoLink: String?
    let hasBus: Bool?
    let organization: Organization?
    let roles: [Role]?
    let bus: PurpleBus?

    enum CodingKeys: String, CodingKey {
        case id
        case roleID = "role_id"
        case fullName, name, email
        case photoURL = "PhotoUrl"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case mobileNo = "MobileNo"
        case age = "Age"
        case gender = "Gender"
        case organizationID = "OrganizationId"
        case location = "Location"
        case checkInLocation = "CheckInLocation"
        case checkOutLocation = "CheckOutLocation"
        case busID = "BusId"
        case stopPointID = "stopPointId"
        case stopPointIDBack = "stopPointIdBack"
        case identification, position, tempStatus, checkIOOrg, isLoggedIn, hasCheckedIn, hasCheckedOut, hasAttended, hasAttendedBack, hasAttendedBySupervisor, hasAttendedBackBySupervisor, hasArrivedBySupervisor
        case photoLink = "PhotoLink"
        case hasBus = "HasBus"
        case organization = "Organization"
        case roles, bus
    }
}

// MARK: - PurpleBus
struct PurpleBus: Codable {
    let id: Int?
    let busNumber: String?
    let capacity, routeID, organizationID, supervisorID: Int?
    let driverID, isactive: Int?
    let createdAt, updatedAt: String?
    let route: Route?
    let organization: Organization?
    let users: [Driver]?
    let supervisor, driver: Driver?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case busNumber, capacity
        case routeID = "routeId"
        case organizationID = "organizationId"
        case supervisorID = "supervisorId"
        case driverID = "driverId"
        case isactive
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case route, organization, users, supervisor, driver
    }
}

// MARK: - StopPoint
struct StopPoint: Codable {
    let id, routeID: Int?
    let lat, lng, northEastLat, northEastLng: Double?
    let southWestLat, southWestLng: Double?
    let minsfromprevious, direction: Int?
    let createdAt, updatedAt: RouteCreatedAt?
    let stopPointID: Int?
    let users, usersBack: [Driver]?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case routeID = "routeId"
        case lat, lng
        case northEastLat = "northEast_lat"
        case northEastLng = "northEast_lng"
        case southWestLat = "southWest_lat"
        case southWestLng = "southWest_lng"
        case minsfromprevious, direction
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case stopPointID = "stopPointId"
        case users
        case usersBack = "users_back"
    }
}

// MARK: - Route
struct Route: Codable {
    let id: Int?
    let name: RouteName?
    let routePath, routePathBack: [StopPoint]?
    let pickupLocation, pickupLocationBack, dropoffLocation, dropoffLocationBack: DropoffLocation?
    let timeToDest: Int?
    let createdAt, updatedAt: RouteCreatedAt?
    let stopPoints: [StopPoint]?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name, routePath, routePathBack, pickupLocation, pickupLocationBack, dropoffLocation, dropoffLocationBack, timeToDest
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case stopPoints = "stop_points"
    }
}

// MARK: - DriverBus
struct DriverBus: Codable {
    let id: Int?
    let busNumber: String?
    let capacity, routeID, organizationID, supervisorID: Int?
    let driverID, isactive: Int?
    let createdAt, updatedAt: String?
    let route: Route?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case busNumber, capacity
        case routeID = "routeId"
        case organizationID = "organizationId"
        case supervisorID = "supervisorId"
        case driverID = "driverId"
        case isactive
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case route
    }
}

// MARK: - Driver
struct Driver: Codable {
    let id, roleID: Int?
    let fullName, name, email, photoURL: String?
    let createdAt, updatedAt, mobileNo: String?
    let age, gender, organizationID: Int?
    let location, checkInLocation, checkOutLocation: String?
    let busID, stopPointID, stopPointIDBack: Int?
    let identification, position: String?
    let tempStatus, checkIOOrg, isLoggedIn: Int?
    let hasAttendedBySupervisor, hasAttendedBackBySupervisor, hasAttended, hasAttendedBack: Bool?
    let hasArrivedBySupervisor, hasCheckedIn, hasCheckedOut: Bool?
    let photoLink: String?
    let hasBus: Bool?
    let organization: Organization?
    let bus: DriverBus?
    let connectionID: String?
    let roles: [Role]?

    enum CodingKeys: String, CodingKey {
        case id
        case roleID = "role_id"
        case fullName, name, email
        case photoURL = "PhotoUrl"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case mobileNo = "MobileNo"
        case age = "Age"
        case gender = "Gender"
        case organizationID = "OrganizationId"
        case location = "Location"
        case checkInLocation = "CheckInLocation"
        case checkOutLocation = "CheckOutLocation"
        case busID = "BusId"
        case stopPointID = "stopPointId"
        case stopPointIDBack = "stopPointIdBack"
        case identification, position, tempStatus, checkIOOrg, isLoggedIn, hasAttendedBySupervisor, hasAttendedBackBySupervisor, hasAttended, hasAttendedBack, hasArrivedBySupervisor, hasCheckedIn, hasCheckedOut
        case photoLink = "PhotoLink"
        case hasBus = "HasBus"
        case organization = "Organization"
        case bus
        case connectionID = "connectionId"
        case roles
    }
}

enum RouteCreatedAt: String, Codable {
    case the20180407103252 = "2018-04-07 10:32:52"
}

// MARK: - DropoffLocation
struct DropoffLocation: Codable {
    let lat, lng: Double?
}

enum RouteName: String, Codable {
    case bolaRoute = "Bola Route"
}

// MARK: - Organization
struct Organization: Codable {
    let id: Int?
    let name: Name?
    let busesCount, usersCount: Int?
    let phoneNumber: String?
    let permitCheckIn, driverAsSupervisor: Int?
    let lat, lng, northEastLat, northEastLng: Double?
    let southWestLat, southWestLng: Double?
    let createdAt, updatedAt: OrganizationCreatedAt?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case busesCount = "BusesCount"
        case usersCount = "UsersCount"
        case phoneNumber, permitCheckIn, driverAsSupervisor, lat, lng
        case northEastLat = "northEast_lat"
        case northEastLng = "northEast_lng"
        case southWestLat = "southWest_lat"
        case southWestLng = "southWest_lng"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

enum OrganizationCreatedAt: String, Codable {
    case the20180407102350 = "2018-04-07 10:23:50"
}

enum Name: String, Codable {
    case bola = "Bola"
}

// MARK: - Role
struct Role: Codable {
    let id: Int?
    let name: RoleName?
    let createdAt, updatedAt: RoleCreatedAt?
    let pivot: Pivot?

    enum CodingKeys: String, CodingKey {
        case id, name
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case pivot
    }
}

enum RoleCreatedAt: String, Codable {
    case the20170812140000 = "2017-08-12 14:00:00"
}

enum RoleName: String, Codable {
    case drivers = "Drivers"
    case supervisors = "Supervisors"
    case users = "Users"
}

// MARK: - Pivot
struct Pivot: Codable {
    let userID, roleID: Int?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case roleID = "role_id"
    }
}
