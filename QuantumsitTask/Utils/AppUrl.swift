//
//  AppUrl.swift
//  MariaTradeSwift
//
//  Created by Mohamed Elmaazy on 2/4/20.
//  Copyright © 2020 Mohamed Elmaazy. All rights reserved.
//

import Foundation

class AppUrl {
    
    static let instance = AppUrl()
    private init () {}
    
 
    fileprivate let BASE_URL = "http://inaclick.online/mtc/"
    
    func getloginUrl() -> String {
        "\(BASE_URL)account/checkCredentials"
    }
    
    func getAboutUsUrl() -> String {
        "\(BASE_URL)aboutus/aboutUs"
    }
    
}
