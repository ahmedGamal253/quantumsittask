//
//  AppUtils.swift
//  MariaTradeSwift
//
//  Created by Mohamed Elmaazy on 2/4/20.
//  Copyright © 2020 Mohamed Elmaazy. All rights reserved.
//

import Foundation
import UIKit

func GET_DEFAULT_HEADERS() -> [String: String] {
    print("Bearer ")
    return ["Content-Type": "application/json",
            "Accept": "application/json"]
}

func GET_RATIO(_ number: CGFloat) -> CGFloat {
    return number * UIScreen.main.bounds.width / 360
}


