//
//  AppPopUpHandler.swift
//  Rateb
//
//  Created by Ebrahim on 6/3/18.
//  Copyright © 2018 Ebrahim. All rights reserved.
//

import Foundation
import PopupDialog

class AppPopUpHandler {
    
    static let instance = AppPopUpHandler()
    private init () {}
    
    func initAboutusPopUp(container:UIViewController, about:String) {
        let vc = AboutUsPopupViewController(About: about)
        let popup = PopupDialog(viewController: vc, buttonAlignment: .horizontal, transitionStyle: .zoomIn, preferredWidth: GET_RATIO(300) , tapGestureDismissal: false)
        container.present(popup, animated: true, completion: nil)
    }
    
}
