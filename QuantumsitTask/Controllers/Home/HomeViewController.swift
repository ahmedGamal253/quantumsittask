//
//  ViewController.swift
//  QuantumsitTask
//
//  Created by jimmy on 11/26/20.
//  Copyright © 2020 jimmy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMaps

class HomeViewController: UIViewController {
    
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var imageInfo: UIImageView!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var labelError: UILabel!
    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var viewMap: GMSMapView!
    
    let viewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initGestures()
        subscribeToConnection()
        viewModel.makeLogin()
        subscribeToResponseAbout()
        subscribeToResponseStopPoints()
        subscribeToResponseRouteBus()
        subscribeToPathRoute()
        initMap()
        
        // Do any additional setup after loading the view.
    }
    
    private func initGestures(){
        imageInfo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getAboutUs)))
    }
    
    @objc func getAboutUs(){
        viewModel.getAboutUs()
    }
    
    private func subscribeToConnection(){
        viewModel.hiddenMap.bind(to: viewMap.rx.isHidden).disposed(by: disposeBag)
        viewModel.hiddenErronInConnection.bind(to: viewError.rx.isHidden).disposed(by: disposeBag)
        viewModel.TextError.bind(to: labelError.rx.text).disposed(by: disposeBag)
        viewModel.showLoader.subscribe(onNext: { (show) in
            self.showIndicator(show)
        }).disposed(by: disposeBag)
        viewModel.showErrorAlart.subscribe(onNext: { (error) in
            if error != ""{
                self.showAlart(message: error)
            }
        }).disposed(by: disposeBag)
    }
    
    private func subscribeToResponseStopPoints(){
        viewModel.stopPointsResponse.subscribe(onNext: { (stopArray) in
            self.addMarkers(arrayLocations: stopArray, color: #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1))
        }).disposed(by: disposeBag)
    }
    
    private func subscribeToResponseRouteBus(){
        viewModel.routPathResponse.subscribe(onNext: { (routeArray) in
            self.addMarkers(arrayLocations: routeArray,color: .red)
        }).disposed(by: disposeBag)
    }
    
    private func subscribeToResponseAbout(){
        viewModel.aboutUsResponse.subscribe(onNext: { (model) in
            AppPopUpHandler.instance.initAboutusPopUp(container: self, about: model.content ?? "")
        }).disposed(by: disposeBag)
    }
    
    private func subscribeToPathRoute(){
        viewModel.pathRoute.subscribe(onNext: { (pathLine) in
            self.drawPath(from: pathLine)
        }).disposed(by: disposeBag)
    }
    
    func initMap() {
        viewMap.isMyLocationEnabled = true
        viewMap.clear()
        viewMap.isMyLocationEnabled = true
    }
    
    func addMarkers(arrayLocations:[StopPoint], color: UIColor){
        var markers = [GMSMarker]()
        for object in arrayLocations {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: object.lat ?? 0 , longitude: object.lng ?? 0)
            marker.icon = GMSMarker.markerImage(with: color)
            marker.map = viewMap
            marker.appearAnimation = .pop
            markers.append(marker)
        }
        let bounds = markers.reduce(GMSCoordinateBounds()) {
            $0.includingCoordinate($1.position)
        }
        viewMap.animate(with: .fit(bounds, withPadding: 30.0))
        viewMap.sizeToFit()
    }
    
    
    func drawPath(from polyStr: String){
        DispatchQueue.main.async {
            let path = GMSPath(fromEncodedPath: polyStr)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.strokeColor = .black
            polyline.map = self.viewMap // Google MapView
            self.viewMap.animate(toZoom: 16)
        }
    }
}

