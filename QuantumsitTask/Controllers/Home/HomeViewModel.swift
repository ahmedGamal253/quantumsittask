//
//  HomeViewModel.swift
//  QuantumsitTask
//
//  Created by jimmy on 11/27/20.
//  Copyright © 2020 jimmy. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import GoogleMaps

class HomeViewModel{
    
    var hiddenMap = BehaviorRelay(value: true)
    var showLoader = BehaviorRelay(value: false)
    var TextError = BehaviorRelay(value: "")
    var hiddenErronInConnection = BehaviorRelay(value: true)
    var aboutUsResponse = PublishSubject<AboutModel>()
    var showErrorAlart = BehaviorRelay(value: "")
    var stopPointsResponse = PublishSubject<[StopPoint]>()
    var routPathResponse = PublishSubject<[StopPoint]>()
    var pathRoute = BehaviorRelay(value: "")
    
    func makeLogin() {
        showLoader.accept(true)
        hiddenMap.accept(true)
        hiddenErronInConnection.accept(true)
        var params = [String:String]()
        params.updateValue("bola_d", forKey: "name")
        params.updateValue("1234", forKey: "password")
        AppConnectionsHandler.post(url:AppUrl.instance.getloginUrl(), params: params, headers: GET_DEFAULT_HEADERS(), type: DataResponsModel.self) {[weak self] (status, model, error) in
            guard let self = self else {return}
            self.showLoader.accept(false)
            switch status {
            case .sucess:
                let model = model as! DataResponsModel
                guard let stopArray = model.innerData?.user?.bus?.route?.stopPoints else {
                    self.hiddenErronInConnection.accept(false)
                    self.TextError.accept("error in connection")
                    return
                }
                guard let RoutArray = model.innerData?.user?.bus?.route?.routePath else {
                    self.hiddenErronInConnection.accept(false)
                    self.TextError.accept("error in connection")
                    return
                }
                self.hiddenMap.accept(false)
                self.stopPointsResponse.onNext(stopArray)
                self.routPathResponse.onNext(RoutArray)
                self.drow(array: stopArray)
                break
            case .errorFromServer:
                self.hiddenErronInConnection.accept(false)
                self.TextError.accept(error ?? "")
                break
            case .errorFromInternet:
                self.hiddenErronInConnection.accept(false)
                self.TextError.accept("error in connection")
                break
            }
        }
    }
    
    func getAboutUs(){
        showLoader.accept(true)
        AppConnectionsHandler.get(url: AppUrl.instance.getAboutUsUrl() , headers: GET_DEFAULT_HEADERS(), type: AboutUsResponseModel.self) { (status, model, error) in
            self.showLoader.accept(false)
            switch status {
            case .sucess:
                let model = model as! AboutUsResponseModel
                guard let about = model.InnerData?[0] else {return self.showErrorAlart.accept("error in connection")}
                self.aboutUsResponse.onNext(about)
                break
            case .errorFromServer:
                self.showErrorAlart.accept(error ?? "")
                break
            case .errorFromInternet:
                self.showErrorAlart.accept("error in connection")
                break
            }
        }
    }
    
   private func drow(array:[StopPoint]){
        for (i,point) in array.enumerated(){
            if i == array.count - 1{break}
            let source = CLLocationCoordinate2D(latitude: point.lat ?? 0 , longitude: point.lng ?? 0)
            let disPoint = array[i+1]
            let distenation  = CLLocationCoordinate2D(latitude: disPoint.lat ?? 0 , longitude: disPoint.lng ?? 0)
            fetchRoute(from: source, to: distenation)
        }
    }
    
  private  func fetchRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) {
        
        let session = URLSession.shared
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving&key=AIzaSyAbWizFYfgCz6awoGIUy6nwpxXb_XiQYuM")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            guard let jsonResult = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] else {
                self.showErrorAlart.accept("error in connection")
                return
            }
            
            guard let routes = jsonResult["routes"] as? [Any] else {
                self.showErrorAlart.accept("error in connection")
                return
            }
            
            guard let route = routes[0] as? [String: Any] else {
                self.showErrorAlart.accept("error in connection")
                return
            }
            
            guard let overview_polyline = route["overview_polyline"] as? [String: Any] else {
                self.showErrorAlart.accept("error in connection")
                return
            }
            
            guard let polyLineString = overview_polyline["points"] as? String else {
                self.showErrorAlart.accept("error in connection")
                return
            }
            //Call this method to draw path on map
            self.pathRoute.accept(polyLineString)
        })
        task.resume()
    }
}
