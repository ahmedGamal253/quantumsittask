//
//  AboutUsPopupViewController.swift
//  QuantumsitTask
//
//  Created by jimmy on 11/27/20.
//  Copyright © 2020 jimmy. All rights reserved.
//

import UIKit

class AboutUsPopupViewController: UIViewController {
    @IBOutlet weak var textAbout: UITextView!
    
    var aboutUs = ""
    init(About:String) {
        aboutUs = About
        super.init(nibName: "AboutUsPopupViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textAbout.stringFromHtml(htmlString: aboutUs)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}
